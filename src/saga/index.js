import { takeEvery } from 'redux-saga/effects';

import { types as cryptoReducer } from '../reducers/cryptoReducer';
import { sagas as cryptoSaga } from './cryptoSaga';

export default function* rootSaga() {
  yield [
    takeEvery(cryptoReducer.GETCRYPTORECORDS, cryptoSaga.getCryptoRecords),
    takeEvery(cryptoReducer.GETSELECTEDRECORD, cryptoSaga.getSelectedRecord),
  ];
}
