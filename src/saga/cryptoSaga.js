import { put, call, select } from 'redux-saga/effects';
import { getRecords } from '../api/crypto';
import { showSuccess, showError } from '../utils/Toast';
import { actions as cryptoActions } from '../reducers/cryptoReducer';

function* getCryptoRecords() {
  const res = yield call(getRecords);
  if(res.length > 0) {
    res.forEach((record, i) => {
      record.key = i
    });
    showSuccess('Records fetched successfully!')
    yield put(cryptoActions.setCryptoRecords(res));
  } else {
    showError('Records fetched Failed!')
    yield put(cryptoActions.setCryptoRecords([]));
  }
}

function* getSelectedRecord(action){
  const state = yield select();
  if(state.cryptoReducer.get('cryptoRecords').length > 0) {
    const selectedRecord = state.cryptoReducer.get('cryptoRecords')[action.key];
    yield put(cryptoActions.setCurrentRecord(selectedRecord));
    yield put(cryptoActions.showRecordModalVisbility(true));
  }
}

export const sagas = {
  getCryptoRecords,
  getSelectedRecord
};
