import request from '../utils/request';
import { BaseUrl } from '../utils/constants';

export const getRecords = () => request('get', `${BaseUrl}/v1/ticker/`);
