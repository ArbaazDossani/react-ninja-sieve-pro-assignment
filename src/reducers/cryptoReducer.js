import { fromJS } from 'immutable';
// import moment from 'moment';
// Actions
export const types = {
  INITIAL: 'app/cryptioActions/INITIAL',
  GETCRYPTORECORDS: 'app/cryptioActions/GETCRYPTORECORDS',
  SETCRYPTORECORDS: 'app/cryptioActions/SETCRYPTORECORDS',
  GETSELECTEDRECORD: 'app/cryptioActions/GETSELECTEDRECORD',
  SETCURRENTRECORD: 'app/cryptioActions/SETCURRENTRECORD',
  SHOWRECORDMODAL: 'app/cryptioActions/SHOWRECORDMODAL',
}

// Reducer
export const initialState = fromJS({
  cryptoRecords: [],
  selectedRecord: null,
  showRecordModal: false,
})

export default (state = initialState, action) => {
  switch (action.type) {
    case types.INITIAL:
      return initialState;
    case types.SETCRYPTORECORDS:
      return state
        .set('cryptoRecords', action.data);
    case types.SHOWRECORDMODAL:
      return state
        .set('showRecordModal', action.bool);
    case types.SETCURRENTRECORD:
      return state
        .set('selectedRecord', action.data);
    default:
      return state;
  }
};

// Action Creators
export const actions = {
  init: () => ({ type: types.INITIAL }),
  getCryptoRecords: () => ({ type: types.GETCRYPTORECORDS }),
  setCryptoRecords: (data) => ({ type: types.SETCRYPTORECORDS, data }),
  getSelectedRecord: (key) => ({ type: types.GETSELECTEDRECORD, key }),
  setCurrentRecord: (data) => ({ type: types.SETCURRENTRECORD, data }),
  showRecordModalVisbility: (bool) => ({ type: types.SHOWRECORDMODAL, bool }),
};
