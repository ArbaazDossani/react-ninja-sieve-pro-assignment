import axios from 'axios';


export default (type, requestUrl, requestedData) => {
  return axios({
    method: type,
    url: requestUrl,
    data: requestedData,
  })
  .then((res) => {
    return res.data
  })
  .catch((err) => {
    if (!err.server) {
      return { err: { status: 500, message: 'Server Error' } };
    }
    return { err };
  });
}
