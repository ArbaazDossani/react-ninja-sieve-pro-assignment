import React from 'react';

let baseUrl;

if (process.env.NODE_ENV === 'production'){
  baseUrl = 'https://api.coinmarketcap.com'; //TODO Put Sieve Pro Production URL here
} else {
  baseUrl = 'https://api.coinmarketcap.com';
}

export const columns = [{
  title: 'Sr No',
  dataIndex: 'key',
  key: 'key',
  render: (text, row) => <a> {row.key + 1} </a>
},{
  title: 'Key',
  dataIndex: 'id',
  key: 'id',
},{
  title: 'Name',
  dataIndex: 'name',
  key: 'name',
}, {
  title: 'Symbol',
  dataIndex: 'symbol',
  key: 'symbol',
}, {
  title: 'Price (in USD)',
  dataIndex: 'price_usd',
  key: 'price_usd',
}, {
  title: 'Market Cap (in USD)',
  dataIndex: 'market_cap_usd',
  key: 'market_cap_usd',
  render: (text, row) => <span> {parseInt(row.market_cap_usd, 16)} </span>
}];

export const updateTimeThreshold = 300000; // 5 minutes in ms

export const BaseUrl = baseUrl;
