import Notification from 'antd/lib/notification';

export const showError = (title, message) => {
  Notification.error({
    message: title || 'Error',
    description: message,
  });
};

export const showSuccess = (message) => {
  Notification.success({
    message: 'Success',
    description: message,
  });
};
