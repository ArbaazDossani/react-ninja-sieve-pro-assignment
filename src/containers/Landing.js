import React from 'react';
import { connect } from 'react-redux';
import { actions as cryptoActions } from '../reducers/cryptoReducer';
import Table from 'antd/lib/table';
import Modal from 'antd/lib/modal';
import Card from 'antd/lib/card';
import { columns, updateTimeThreshold } from '../utils/constants';


class Landing extends React.Component{
  componentWillMount() {
    this.toggleModalVisbility(false);
    this.props.getCryptoRecords();
    this.timer = setInterval(()=> this.props.getCryptoRecords(), updateTimeThreshold);
  }
  toggleModalVisbility(bool) {
    this.props.showRecordModalVisbility(bool);
  }
  render() {
    const { selectedRecord, showRecordModal } = this.props;
    const CardView = selectedRecord && (
      <Card>
        <p>Title: {selectedRecord.name}</p>
        <p>Price (in USD): {selectedRecord.price_usd}</p>
        <p>Symbol: {selectedRecord.symbol}</p>
        <p>Market Cap(in USD): {selectedRecord.market_cap_usd}</p>
      </Card>
    );
    const ModalView = (
      <Modal
        title={null}
        visible={showRecordModal}
        footer={null}
        onCancel={() => this.toggleModalVisbility(false)}
      >
      {CardView}
      </Modal>
    );
    const TableView = (
      <Table
        dataSource={this.props.cryptoRecords}
        columns={columns}
        onRowClick={(column) => { this.props.getSelectedRecord(column.key) }}
      />
    )
    return (
      <div>
      {
        this.props.cryptoRecords.length > 0 ?
        <span>
          <h2> Cryptocurrency Market Capitalization </h2>
          {TableView}
          {ModalView}
        </span>
        :
        <span> Loading Please Wait </span>
      }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  cryptoRecords: state.cryptoReducer.get('cryptoRecords'),
  selectedRecord: state.cryptoReducer.get('selectedRecord'),
  showRecordModal: state.cryptoReducer.get('showRecordModal'),
})

const mapDispatchToProps = (dispatch) => ({
  getCryptoRecords: () => dispatch(cryptoActions.getCryptoRecords()),
  getSelectedRecord: (key) => dispatch(cryptoActions.getSelectedRecord(key)),
  showRecordModalVisbility: (bool) => dispatch(cryptoActions.showRecordModalVisbility(bool)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
