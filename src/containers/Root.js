import React from 'react';
import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux';
import { autoRehydrate, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga'
import immutableTransform from 'redux-persist-transform-immutable';
import { createBlacklistFilter } from 'redux-persist-transform-filter';
import App from './App';
import reactNinjaApp from '../reducers';
import rootSaga from '../saga';

import Layout from 'antd/lib/layout';
const { Content } = Layout;

const sagaMiddleware = createSagaMiddleware();
let store = createStore(
  reactNinjaApp,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  compose(
    applyMiddleware(sagaMiddleware),
    autoRehydrate()
  )
)

const saveSubsetBlacklistFilter = createBlacklistFilter(
  reactNinjaApp,
  ['showRecordModal']
);

sagaMiddleware.run(rootSaga);

class Root extends React.Component{

  constructor(){
    super();
    this.state={
      initialized: false
    }
  }

  componentWillMount() {
    persistStore(store, {transforms: [immutableTransform(), saveSubsetBlacklistFilter]}, () => {
      this.setState({
        initialized: true,
      })
    });
  }

  render() {
    return (
      <Provider store={store}>
        <div>
          {
            this.state.initialized &&
            <Layout>
              <Content style={{ backgroundColor: '#fff' }}>
                <App />
              </Content>
            </Layout>
          }
        </div>
      </Provider>
    )
  }
}

export default Root;
