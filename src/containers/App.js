import React from 'react';
import {
  Router,
  Route,
} from 'react-router-dom';
import './App.css';
import history from '../utils/history';
import Landing from './Landing';

const App = () => (
  <Router history={history}>
    <div>
      <Route exact path="/" component={Landing} />
    </div>
  </Router>
)

export default App;
