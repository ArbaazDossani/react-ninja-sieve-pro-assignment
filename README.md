This project was created as an Assignment for Sieve Pro Hiring Challenge (React-Ninja Task No. 5) with Create-React-App Boilerplate

The functionalities implemented are as follows:

* Retrieve and Display Paginated Table View for Cryptocurrency and their respective details.
* On click of every row, Display Modal of Detailed view of that particular Cryptocurrency.
* At every interval of updateTimeThreshold (here, 5min i.e. 300000ms), repeat Step 1

Alignmemnt to Task Requirements:

* Correct implementation of all the functionalities requested (listed above).
* Usage of React, Redux, Redux-saga (Additionally, using persist and transform filters for enhancing redux ability on page reload).
* Deployment to Remote Repository (Bitbucket)
* Usage of AntDesign for Paginated Table, Modal, Card components with their minimal styling Requirements.

How to run the code:

* Git clone the project. i.e. git clone https://ArbaazDossani@bitbucket.org/ArbaazDossani/react-ninja-sieve-pro-assignment.git
* cd react-ninja-sieve-pro-assignment
* Run a simple command: npm run react-ninja
The above command installs all the required node_modules and initiates the react-scripts for start script. (i.e run the server in development mode on port 3000)

